# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta
from trytond.pyson import Not, Bool, Eval
from trytond.modules.company_office.office import OfficeRelationMixin


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    offices = fields.Many2Many('product.template-company.office', 'template',
        'office', 'Offices',
        states={
            'readonly': Not(Bool(Eval('active')))
        },
        depends=['active'])


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'


class TemplateOffice(OfficeRelationMixin, ModelSQL):
    '''Product Template - Office'''
    __name__ = 'product.template-company.office'

    template = fields.Many2One('product.template', 'Template', required=True,
        select=True, ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._relation_model_field = 'template'
