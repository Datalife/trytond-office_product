# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_office_product import suite

__all__ = ['suite']
