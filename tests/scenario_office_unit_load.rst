=========================
Office Unit Load Scenario
=========================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_unit_load.tests.tools import \
    ...     create_unit_load


Install office_product::

    >>> config = activate_modules(['office_product', 'stock_unit_load'])


Create company::

    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create new user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> user = User()
    >>> user.name = 'User 1'
    >>> user.login = 'user1'
    >>> user.companies.append(company)
    >>> user.company = Company(company.id)
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> user.groups.append(stock_group)
    >>> user.save() 


Create office::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.users.append(admin)
    >>> office1.save()


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('40')
    >>> template.offices.append(office1)
    >>> template.save()
    >>> product, = template.products


Create unit load::

    >>> ul = create_unit_load(config, product=product)
    >>> UnitLoad = Model.get('stock.unit_load')
    >>> Move = Model.get('stock.move')
    >>> len(UnitLoad.find([]))
    1
    >>> len(Move.find([]))
    1

Change user::

    >>> config.user = user.id
    >>> len(UnitLoad.find([]))
    0
    >>> len(Move.find([]))
    0
