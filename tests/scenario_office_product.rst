=======================
Office Product Scenario
=======================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install office_product::

    >>> config = activate_modules('office_product')


Create company::

    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create new user::

    >>> User = Model.get('res.user')
    >>> user = User()
    >>> user.name = 'User 1'
    >>> user.login = 'user1'
    >>> user.companies.append(company)
    >>> user.company = Company(company.id)
    >>> user.save() 


Create office::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.save()
    >>> product, = template.products


Add office to product::

    >>> template.offices.append(office1)
    >>> template.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.AccessError: You are not allowed to write to records "1" of "Product Template" because of at least one of these rules:
    Offices in user - 

    >>> office1 = Office(office1.id)
    >>> admin.offices.append(office1)
    >>> admin.save()
    >>> office1 = Office(office1.id)
    >>> template.reload()
    >>> template.offices.append(office1)
    >>> template.save()
    >>> len(product.offices)
    1
    >>> len(ProductTemplate.find([]))
    1


Change user::

    >>> config.user = user.id
    >>> len(ProductTemplate.find([]))
    0